# IAM - Project

# Getting Started
## The Identity Management software
The main goal is to manage users of an Information System. The application will be able to :
    1. Access, create and modify the user information.
    2. Persist users data in a database (DERBY).
    3. The interface used for this application is console.

## Prerequisites
Java (JDK & JRE) installed in the system.
Eclipse Java IDE for Web Developers.
Apache Derby Database.

# Version
1.0

# Author
Sitharaman Deepak Guptha