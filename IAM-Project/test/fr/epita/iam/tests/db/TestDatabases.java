package fr.epita.iam.tests.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.epita.iam.datamodel.Identity;
import fr.epita.iam.services.conf.ConfKey;
import fr.epita.iam.services.conf.ConfigurationService;

/**
 * <h1>TestDatabasess</h1>
 * <p>
 * This class contains the test cases of create, update, delete and search
 * operations
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public class TestDatabases {

	/**
	 * Executing the testConnection, insert, update, delete and search methods
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		testConnection();

		insertQuery();

		updateQuery();

		deleteQuery();

		searchQuery();

	}

	/**
	 * The method is used for the insert query.
	 */
	private static void insertQuery() {
		try {
			final Connection connection = getConnection();
			final PreparedStatement pstmt = connection.prepareStatement(
					"INSERT INTO IAMPROJECT.IDENTITIES (IDENTITY_DISPLAYNAME, IDENTITY_EMAIL, IDENTITY_UID) VALUES (?, ?, ?)");
			pstmt.setString(1, "test");
			pstmt.setString(2, "test@cserr.com");
			pstmt.setString(3, "9123");
			pstmt.execute();
			pstmt.close();
			connection.close();
			System.out.println("\n**********************");
			System.out.println("*    Row Inserted    *");
			System.out.println("**********************\n");
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	/**
	 * The method is used for the update query.
	 */
	private static void updateQuery() {
		try {
			final Connection connection = getConnection();
			final String uid = "9123";
			final PreparedStatement pstmt = connection.prepareStatement(
					"UPDATE IAMPROJECT.IDENTITIES SET IDENTITY_DISPLAYNAME = ?, IDENTITY_EMAIL = ?  WHERE IDENTITY_UID = ?");
			pstmt.setString(1, "test");
			pstmt.setString(2, "test@gmail.com");
			pstmt.setString(3, uid);
			int updateQueryValue = pstmt.executeUpdate();
			if (updateQueryValue == 0) {
				System.out.println("\n**********************");
				System.out.println("*    Check the UID   *");
				System.out.println("**********************\n");
			} else {
				System.out.println("\nRow Updated for the UID: " + uid);
			}
			pstmt.close();
			connection.close();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	/**
	 * The method is used for the delete query.
	 */
	private static void deleteQuery() {
		try {
			final Connection connection = getConnection();
			final String uid = "9123";
			final PreparedStatement pstmt = connection
					.prepareStatement("DELETE FROM IAMPROJECT.IDENTITIES WHERE IDENTITY_UID = ?");
			pstmt.setString(1, uid);
			int deleteQueryValue = pstmt.executeUpdate();
			if (deleteQueryValue == 0) {
				System.out.println("\n**********************");
				System.out.println("*    Check the UID   *");
				System.out.println("**********************\n");
			} else {
				System.out.println("\n**********************");
				System.out.println("*     Row Deleted    *");
				System.out.println("**********************\n");
			}
			pstmt.close();
			connection.close();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	/**
	 * The method is used for the search query.
	 */
	private static void searchQuery() {
		final List<Identity> list = new ArrayList<>();

		Connection connection = null;
		try {
			connection = getConnection();
			final String displayName = "Xyz";
			final String email = "xyz@rrediff.com";
			final PreparedStatement pstmt = connection.prepareStatement(
					"SELECT IDENTITY_DISPLAYNAME, IDENTITY_EMAIL, IDENTITY_UID from IAMPROJECT.IDENTITIES where IDENTITY_DISPLAYNAME like ? or IDENTITY_EMAIL like ?");
			pstmt.setString(1, "%" + displayName + "%");
			pstmt.setString(2, "%" + email + "%");
			final ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				final String displayNameResult = rs.getString("IDENTITY_DISPLAYNAME");
				final String emailResult = rs.getString("IDENTITY_EMAIL");
				final String uidResult = rs.getString("IDENTITY_UID");
				final Identity identity = new Identity(displayNameResult, uidResult, emailResult);
				list.add(identity);
				System.out.println(list);
			} else {
				System.out.println("\n**********************");
				System.out.println("*  Search Not Found  *");
				System.out.println("**********************\n");
			}

			pstmt.close();
			connection.close();
		} catch (final SQLException e) {
			if (connection != null) {
				try {
					connection.close();
				} catch (final SQLException e2) {
					// TODO handle
				}
			}
		}
	}

	/**
	 * This method is used for the test connection
	 * 
	 * @throws Exception
	 */
	private static void testConnection() throws Exception {

		final String currentSchema = "IAMPROJECT";
		final Connection connection = getConnection();
		// Then I should get the "TEST" string in the currentSchema
		if (!currentSchema.equals("IAMPROJECT")) {
			throw new Exception("problem: connection not operational");
		}
	}

	/**
	 * The method is used to connect the database using the DriverManager with
	 * localhost, username and password.
	 * 
	 * @return connection
	 * @throws SQLException
	 */
	private static Connection getConnection() throws SQLException {
		// Given this context
		final String url = "jdbc:derby://localhost:1527/projectDatabase;create=true";
		Connection connection = null;

		// When I connect
		connection = DriverManager.getConnection(url, "iamAdmin", "admin@123");
		return connection;
	}

}
