package fr.epita.iam.tests.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

/**
 * <h1>TestProperties</h1>
 * <p>
 * This class is used to test the properties file and retrieving the keySet
 * values
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public class TestProperties {
	/**
	 * Used to test the properties file and retriving the keySet values.
	 * 
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		final Properties properties = new Properties();

		// Loading the properties file using the FileinputStream class.
		properties.load(new FileInputStream(new File("test/test.properties")));

		// Assigning the keySet values to the set.
		final Set<Object> keySet = properties.keySet();
		System.out.println(properties.getProperty("db.url"));
		System.out.println(keySet);
	}
}
