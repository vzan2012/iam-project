package fr.epita.iam.services.conf;

/**
 * <h1>ConfKey</h1>
 * <p>
 * This is used define the set of enum constants like database url, database
 * username, database password, SQL query statements
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public enum ConfKey {

	/**
	 * Defining the set or the type of keys.
	 */

	BACKEND_MODE("backend.mode"), FALLBACK_BACKEND_MODE("backend.mode"), DB_URL("db.url"), DB_USER(
			"db.user"), DB_PASSWORD("db.pwd"), DB_BACKEND("db"), IDENTITY_SEARCH_QUERY(
					"identity.search"), IDENTITY_INSERT_QUERY("identity.insert"), IDENTITY_DELETE_QUERY(
							"identity.delete"), IDENTITY_UPDATE_QUERY("identity.update"), LOGIN_QUERY("login.user"),;

	/**
	 * Declaring the variable key.
	 */
	private String key;

	/**
	 * The constructor with a private modifier and with a parameter of key.
	 * 
	 * @param key
	 */
	private ConfKey(String key) {
		this.key = key;
	}

	/**
	 * This method is used to return the key value of the String type.
	 * 
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

}
