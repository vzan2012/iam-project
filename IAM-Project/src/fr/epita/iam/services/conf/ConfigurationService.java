package fr.epita.iam.services.conf;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * <h1>ConfigurationService</h1>
 * <p>
 * This class is used for the initialization of the properties file, setter and
 * getter methods
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */

public class ConfigurationService {

	// Initialize the logger
	static Logger logger = LogManager.getLogger(ConfigurationService.class);

	/**
	 * Declaring the variables for the configuration
	 */
	public static final String BACKEND_MODE = "backend.mode";
	public static final String FALLBACK_BACKEND_MODE = "fallback.backend.mode";
	public static final String DB_BACKEND = "db";
	public static final String FILE_BACKEND = "file";

	/**
	 * Declare the Properties class which represents a persistent set of properties.
	 */
	private static Properties properties;

	static {
		init();
	}

	/**
	 * This method used for initialize the properties file.
	 */
	private static void init() {
		try {
			properties = new Properties();
			properties.load(new FileInputStream(new File(System.getProperty("conf.file.path"))));
		} catch (final Exception e) {
			logger.error("Exception caught in: " + e.getMessage());
		}
	}

	/**
	 * This method used to return the key value as Integer.
	 * 
	 * @param key
	 * @return the key as the Integer value
	 */
	public static Integer getIntProperty(ConfKey key) {
		final String valueAsString = getProperty(key);
		return Integer.valueOf(valueAsString);
	}

	/**
	 * This method used to return the key value as String.
	 * 
	 * @param key
	 * @return the properties value of the key
	 */
	public static String getProperty(ConfKey key) {
		return properties.getProperty(key.getKey());
	}
}
