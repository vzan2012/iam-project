package fr.epita.iam.services.conf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.Logger;

import org.apache.logging.log4j.LogManager;

/**
 * @author Deepak Guptha
 *
 */
public class ConfigurationServiceAsSingleton {

	private final Properties properties;
	private static ConfigurationServiceAsSingleton instance;

	// Initialize the logger
	static Logger logger = LogManager.getLogger(ConfigurationServiceAsSingleton.class);

	/**
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private ConfigurationServiceAsSingleton() throws IOException {
		properties = new Properties();
		properties.load(new FileInputStream(new File(System.getProperty("conf.file.path"))));
	}

	/**
	 * @return
	 */
	public static ConfigurationServiceAsSingleton getInstance() {
		if (instance == null) {

			try {
				instance = new ConfigurationServiceAsSingleton();
			} catch (Exception e) {
				logger.error("Error: " + e.getMessage());
			}

		}
		return instance;
	}

	/**
	 * @param key
	 * @return
	 */
	public String getProperty(String key) {
		return properties.getProperty(key);
	}

}
