package fr.epita.iam.services;

import java.io.Serializable;
import java.util.List;

import fr.epita.iam.datamodel.Identity;
import fr.epita.iam.exceptions.EntityCreationException;
import fr.epita.iam.exceptions.EntityDeletionException;
import fr.epita.iam.exceptions.EntityReadException;
import fr.epita.iam.exceptions.EntitySearchException;
import fr.epita.iam.exceptions.EntityUpdateException;

/**
 * <h1>DAO</h1>
 * <p>
 * It is an interface with a collection of abstract methods - create, update,
 * delete, search and Serializable id.
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 * @param <T>
 */
public interface DAO<T> {

	/**
	 * This abstract method is used to perform the create operation.
	 * 
	 * @param entity
	 * @throws EntityCreationException
	 */
	public void create(T entity) throws EntityCreationException;

	/**
	 * This abstract method is used to perform the update operation.
	 * 
	 * @param entity
	 * @throws EntityUpdateException
	 */
	public void update(T entity) throws EntityUpdateException;

	/**
	 * This abstract method is used to perform the delete operation.
	 * 
	 * @param entity
	 * @throws EntityDeletionException
	 */
	public void delete(T entity) throws EntityDeletionException;

	/**
	 * This abstract method is used to retrieve the data using the parameter id.
	 * 
	 * @param id
	 * @return the Serializable id
	 * @throws EntityReadException
	 */
	public Identity getById(Serializable id) throws EntityReadException;

	/**
	 * This abstract method is used for the search operation with list type.
	 * 
	 * @param criteria
	 * @return the list
	 * @throws EntitySearchException
	 */
	public List<Identity> search(T criteria) throws EntitySearchException;

}
