package fr.epita.iam.services.identity;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.epita.iam.datamodel.Identity;
import fr.epita.iam.datamodel.LoginIdentity;
import fr.epita.iam.exceptions.EntityCreationException;
import fr.epita.iam.exceptions.EntityReadException;
import fr.epita.iam.services.conf.ConfKey;
import fr.epita.iam.services.conf.ConfigurationService;
import fr.epita.iam.ui.ConsoleOperations;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * <h1>IdentityJDBCDAO</h1>
 * <p>
 * This class is used for registering the derby connection with the driver, and
 * performing the CRUD operations
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public class IdentityJDBCDAO implements IdentityDAO {

	static {
		IdentityDAOFactoryDynamicRegistration.registeredDAOs.put(ConfKey.DB_BACKEND.getKey(), new IdentityJDBCDAO());
	}

	// Initialize the logger
	static Logger logger = LogManager.getLogger(ConsoleOperations.class);

	static final String START_TAG = "\n***************************";
	static final String FINAL_TAG = "***************************\n";

	/**
	 * @return the connection
	 * @throws SQLException
	 */
	private static Connection getConnection() throws SQLException {
		final String url = ConfigurationService.getProperty(ConfKey.DB_URL);
		Connection connection = null;

		connection = DriverManager.getConnection(url, ConfigurationService.getProperty(ConfKey.DB_USER),
				ConfigurationService.getProperty(ConfKey.DB_PASSWORD));

		return connection;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.epita.iam.services.DAO#create(java.lang.Object)
	 */
	public void create(Identity identity) {
		try {
			Connection connection = null;
			try {
				connection = getConnection();
				final PreparedStatement pstmt = connection
						.prepareStatement(ConfigurationService.getProperty(ConfKey.IDENTITY_INSERT_QUERY));

				pstmt.setString(1, identity.getDisplayName());
				pstmt.setString(2, identity.getEmail());
				pstmt.setString(3, identity.getUid());
				pstmt.execute();
				pstmt.close();
				connection.close();
				logger.info(START_TAG);
				logger.info("*    Identity Inserted    *");
				logger.info(FINAL_TAG);
			} catch (final SQLException e) {
				if (connection != null) {
					try {
						connection.close();
					} catch (final SQLException e1) {
						logger.error("Error: " + e1.getMessage());
					}
				}
				final EntityCreationException exception = new EntityCreationException(identity, e);
				throw exception;
			}
		} catch (Exception e) {
			logger.error("Error in Inserting the Identity: " + e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.epita.iam.services.DAO#delete(java.lang.Object)
	 */
	public void delete(Identity identity) {
		Connection connection = null;
		try {
			connection = getConnection();
			final PreparedStatement pstmt = connection
					.prepareStatement(ConfigurationService.getProperty(ConfKey.IDENTITY_DELETE_QUERY));

			pstmt.setString(1, identity.getUid());
			int deleteQueryValue = pstmt.executeUpdate();
			if (deleteQueryValue == 0) {
				logger.info(START_TAG);
				logger.info("*      Check the UID      *");
				logger.info(FINAL_TAG);
			} else {
				logger.info(START_TAG);
				logger.info("*     Identity Deleted    *");
				logger.info(FINAL_TAG);
			}
			pstmt.close();
			connection.close();

		} catch (Exception e) {
			logger.error("Error in Deleting the Identity: " + e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.epita.iam.services.DAO#update(java.lang.Object)
	 */
	public void update(Identity identity) {
		Connection connection = null;
		try {
			connection = getConnection();
			final PreparedStatement pstmt = connection
					.prepareStatement(ConfigurationService.getProperty(ConfKey.IDENTITY_UPDATE_QUERY));

			pstmt.setString(1, identity.getDisplayName());
			pstmt.setString(2, identity.getEmail());
			pstmt.setString(3, identity.getUid());
			int updateQueryValue = pstmt.executeUpdate();
			if (updateQueryValue == 0) {
				logger.info(START_TAG);
				logger.info("*      Check the UID      *");
				logger.info(FINAL_TAG);
			} else {
				logger.info("\nIdentity Updated for the UID: " + identity.getUid());
			}
			pstmt.close();
			connection.close();

		} catch (Exception e) {
			logger.error("\nError in Updating the Identity: " + e.getMessage());
		}
	}

	/**
	 * This method is used to return the Identity Id
	 * 
	 * @param id
	 * @return the identity
	 */
	public Identity getById(int id) {
		final Identity identity = new Identity();
		return identity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.epita.iam.services.DAO#search(java.lang.Object)
	 */
	public List<Identity> search(Identity criteria) {
		final List<Identity> list = new ArrayList<>();

		Connection connection = null;
		try {
			connection = getConnection();
			final PreparedStatement pstmt = connection
					.prepareStatement(ConfigurationService.getProperty(ConfKey.IDENTITY_SEARCH_QUERY));
			pstmt.setString(1, "%" + criteria.getDisplayName() + "%");
			pstmt.setString(2, "%" + criteria.getEmail() + "%");
			final ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				final String displayName = rs.getString("IDENTITY_DISPLAYNAME");
				final String email = rs.getString("IDENTITY_EMAIL");
				final String uid = rs.getString("IDENTITY_UID");
				final Identity identity = new Identity(displayName, uid, email);
				list.add(identity);
			} else {
				logger.info(START_TAG);
				logger.info("*     Search Not Found    *");
				logger.info(FINAL_TAG);
			}

			pstmt.close();
			connection.close();
		} catch (final SQLException e) {
			if (connection != null) {
				try {
					connection.close();
				} catch (final SQLException e2) {
					logger.info("Error: " + e.getMessage());
				}
			}
		}
		return list;
	}

	/**
	 * @param identity
	 * @return
	 */
	public boolean loginData(LoginIdentity identity) {
		Connection connection = null;
		boolean userStatus = false;
		try {
			connection = getConnection();
			String usernameValue = null;
			String passwordValue = null;

			final PreparedStatement pstmt = connection
					.prepareStatement(ConfigurationService.getProperty(ConfKey.LOGIN_QUERY));

			final ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				usernameValue = rs.getString("USERNAME");
				passwordValue = rs.getString("PASSWORD");

				if (usernameValue.equals(identity.getUsername()) && passwordValue.equals(identity.getPassword())) {
					userStatus = true;
				} else {
					userStatus = false;
				}
			}
			pstmt.close();
			connection.close();

		} catch (Exception e) {
			logger.error("Error in Updating the Identity: " + e.getMessage());
		}
		return userStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.epita.iam.services.DAO#getById(java.io.Serializable)
	 */
	@Override
	public Identity getById(Serializable id) throws EntityReadException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.epita.iam.services.identity.IdentityDAO#healthCheck()
	 */
	@Override
	public boolean healthCheck() {
		try {
			final Connection connection = getConnection();
			connection.close();
			return true;
		} catch (final SQLException sqle) {
			logger.info("\nError: " + sqle.getMessage());
		}
		return false;

	}

}
