package fr.epita.iam.services.identity;

import fr.epita.iam.datamodel.Identity;
import fr.epita.iam.services.DAO;

/**
 * <h1>IdentityDAO</h1>
 * <p>
 * This interface extends the DAO interface and has a boolean method.
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public interface IdentityDAO extends DAO<Identity> {
	/**
	 * Abstract method with boolean return type.
	 * 
	 * @return the boolean
	 */
	boolean healthCheck();
}
