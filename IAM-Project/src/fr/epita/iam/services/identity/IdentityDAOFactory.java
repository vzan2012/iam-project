package fr.epita.iam.services.identity;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import fr.epita.iam.services.conf.ConfKey;
import fr.epita.iam.services.conf.ConfigurationService;
import fr.epita.iam.ui.ConsoleOperations;

/**
 * <h1>IdentityDAOFactory</h1>
 * <p>
 * This class implements the Factory pattern - for getting the property of
 * configuration key
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public class IdentityDAOFactory {

	// Initialize the logger
	static Logger logger = LogManager.getLogger(ConsoleOperations.class);

	/**
	 * Declaring the IdentityDAO and boolean variables.
	 */
	private static IdentityDAO currentInstance;
	private static boolean fallbackActivated;

	/**
	 * This method is used to return the currentInstance.
	 * 
	 * @return the currentInstance
	 * @throws Exception
	 */
	public static IdentityDAO getDAO() throws Exception {

		final String backendMode = ConfigurationService.getProperty(ConfKey.BACKEND_MODE);

		if (currentInstance != null) {
			currentInstance = getInstance(backendMode);
		}

		if (currentInstance != null && !currentInstance.healthCheck()) {
			fallbackActivated = true;
			final String fallbackMode = ConfigurationService.getProperty(ConfKey.FALLBACK_BACKEND_MODE);
			currentInstance = getInstance(fallbackMode);
		}

		return currentInstance;

	}

	/**
	 * This method is used to set the db mode and returns the instance.
	 * 
	 * @param backendMode
	 * @return the instance
	 * @throws Exception
	 */
	private static IdentityDAO getInstance(final String backendMode) throws Exception {
		IdentityDAO instance = null;

		switch (backendMode) {
		case "db":
			try {
				instance = new IdentityJDBCDAO();
			} catch (Exception e) {
				logger.error("Error: " + e.getMessage());
			}
			break;

		default:
			implementException();
		}

		return instance;

	}

	/**
	 * This method is used to implement the exception for the getInstance() method.
	 */
	public static void implementException() {
		try {
			throw new Exception("Not implemented yet.");
		} catch (Exception e) {
			logger.error("Error: " + e.getMessage());
		}
	}
}