package fr.epita.iam.services.identity;

import java.util.LinkedHashMap;
import java.util.Map;

import fr.epita.iam.services.conf.ConfKey;
import fr.epita.iam.services.conf.ConfigurationService;

/**
 * <h1>IdentityDAOFactoryDynamicRegistration</h1>
 * <p>
 * This class contains the getDAO() method to return the backend mode key.
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public class IdentityDAOFactoryDynamicRegistration {

	/**
	 * Declaring the registeredDAOs map variable.
	 */
	static Map<String, IdentityDAO> registeredDAOs = new LinkedHashMap<>();

	/**
	 * This method is used to return the Backend mode key value.
	 * 
	 * @return the ConfigurationService class of the getProperty() method
	 * @throws Exception
	 */
	public static IdentityDAO getDAO() throws Exception {
		return registeredDAOs.get(ConfigurationService.getProperty(ConfKey.BACKEND_MODE));
	}
}
