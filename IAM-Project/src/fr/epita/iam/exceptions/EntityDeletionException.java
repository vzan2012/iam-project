package fr.epita.iam.exceptions;

/**
 * <h1>EntityDeletionException</h1>
 * <p>
 * This class is a custom exception for the EntityDeletionException which
 * extends the Exception class.
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public class EntityDeletionException extends Exception {
	final transient Object entity;

	/**
	 * This constructor has two parameters of entity and cause.
	 * 
	 * @param entity
	 * @param cause
	 */
	public EntityDeletionException(Object entity, Throwable cause) {
		this.entity = entity;
		initCause(cause);
	}

	/**
	 * The method is used to return the custom error message of String data type.
	 * 
	 * @return getUserMessage()
	 */
	public String getUserMessage() {
		return "The following entity deletion has failed :" + entity.toString();
	}

}