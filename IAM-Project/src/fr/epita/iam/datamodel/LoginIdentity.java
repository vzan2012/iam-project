package fr.epita.iam.datamodel;

/**
 * <h1>LoginIdentity</h1>
 * <p>
 * This class is used to assign the username and password using the setter and
 * getter methods.
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public class LoginIdentity {

	private String username;
	private String password;

	/**
	 * The constructor with two parameters of username and password.
	 * 
	 * @param username
	 * @param password
	 */
	public LoginIdentity(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * Default Constructor
	 */
	public LoginIdentity() {

	}

	/**
	 * This method is used to retrieve the username of String dataype.
	 * 
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * This method is used to set the username.
	 * 
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * This method is used to retrieve the password of String dataype.
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * This method is used to set the password.
	 * 
	 * @param password
	 *            password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}