package fr.epita.iam.datamodel;

import java.util.Map;

/**
 * <h1>Identity</h1>
 * <p>
 * This class is used to assign the displayName, uid and email address using the
 * setter and getter methods.
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 */

public class Identity {

	private String displayName;
	private String uid;
	private String email;
	private Map<String, String> attributes;

	/**
	 * 
	 * Default Constructor
	 * 
	 */
	public Identity() {

	}

	/**
	 * The constructor with the three parameters of displayName, uid and email
	 * address.
	 * 
	 * @param displayName
	 * @param uid
	 * @param email
	 */
	public Identity(String displayName, String uid, String email) {
		this.displayName = displayName;
		this.uid = uid;
		this.email = email;
	}

	/**
	 * The method is used to return the display name of String data type.
	 * 
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * The method is used to set the display name.
	 * 
	 * @param displayName
	 *            the displayName to set
	 * 
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * The method is used to return the uid of String data type.
	 * 
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * The method is used to set the uid.
	 * 
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * The method is used to return the email address of String data type.
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * The method is used to set the email address of String data type.
	 * 
	 * @param email
	 *            the email is set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * The method is used to return the attributes of Map.
	 * 
	 * @return the attributes
	 */
	public Map<String, String> getAttributes() {
		return attributes;
	}

	/**
	 * The method is used to set the attributes of Map.
	 * 
	 * @param attributes
	 *            the attributes is set
	 */
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Identity [displayName=" + displayName + ", uid=" + uid + ", email=" + email + ", attributes="
				+ attributes + "]";
	}

}