package fr.epita.iam.launcher;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import fr.epita.iam.datamodel.LoginIdentity;
import fr.epita.iam.services.identity.IdentityJDBCDAO;
import fr.epita.iam.ui.ConsoleOperations;

/**
 * <h1>Launcher</h1>
 * <p>
 * This file is used to launch the application with the VM arguments of
 * -Dconf.file.path="resources/db.properties"
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */

public class Launcher {

	/**
	 * The launcher is used to launch the application.
	 * 
	 * @param args
	 * 
	 */
	public static void main(String[] args) {
		// Initialize resources
		final LoginIdentity loginIdentity;
		final IdentityJDBCDAO dao = new IdentityJDBCDAO();
		final ConsoleOperations console = new ConsoleOperations();

		// Initialize the logger
		final Logger logger = LogManager.getLogger(Launcher.class);

		// Welcome
		console.welcomeProject();

		// Authentication
		loginIdentity = console.readLoginfromConsole();
		boolean userLoginStatus = dao.loginData(loginIdentity);

		// Menu
		if (userLoginStatus == true) {
			logger.info("\n\n******************************");
			logger.info("*      Login Successful      *");
			logger.info("******************************\n");
			console.displayMenu();
			console.userOperationMenu();
		} else {
			logger.info("\n\n******************************");
			logger.info("*  Invalid user credentials  *");
			logger.info("******************************");
		}

		// Release resources
		dao.healthCheck();
	}

}