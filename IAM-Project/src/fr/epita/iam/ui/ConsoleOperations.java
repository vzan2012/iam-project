package fr.epita.iam.ui;

import java.util.List;
import java.util.Scanner;

import fr.epita.iam.datamodel.Identity;
import fr.epita.iam.datamodel.LoginIdentity;
import fr.epita.iam.services.identity.IdentityJDBCDAO;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * <h1>ConsoleOperations</h1>
 * <p>
 * This class file has the methods for getting the inputs from the user, welcome
 * message, user menus and closing of scanner connections.
 * </p>
 * 
 * @author Deepak Guptha
 * @version 1.0
 *
 */
public class ConsoleOperations {

	private final Scanner scanner;

	// Initialize the logger
	static Logger logger = LogManager.getLogger(ConsoleOperations.class);

	static final String START_TAG = "\n**********************";
	static final String FINAL_TAG = "**********************\n";

	/**
	 * Default Constructor with the scanner class, which the allows the user to read
	 * values of various types.
	 */
	public ConsoleOperations() {
		scanner = new Scanner(System.in);
	}

	/**
	 * Getting the user inputs from the console for the creation of record.
	 * 
	 * @return the identity for the readIdentityFromConsole() method
	 */
	public Identity readIdentityFromConsole() {
		final Identity identity = new Identity();
		logger.info("Please enter the Display Name");
		String line = scanner.nextLine();
		identity.setDisplayName(line);
		logger.info("Please enter the Email ID");
		line = scanner.nextLine();
		identity.setEmail(line);
		logger.info("Please enter the UID");
		line = scanner.nextLine();
		identity.setUid(line);
		return identity;
	}

	/**
	 * Getting the user inputs from the console for searching the record.
	 * 
	 * @return the identity for the readCriteriaFromConsole() method
	 */
	public Identity readCriteriaFromConsole() {
		logger.info("Enter criteria");
		final Identity identity = new Identity();
		logger.info("Please input the criterion for the Display Name: ");
		String line = scanner.nextLine();
		identity.setDisplayName(line);
		logger.info("Please input the criterion for Email ID");
		line = scanner.nextLine();
		identity.setEmail(line);

		return identity;
	}

	/**
	 * Getting the user inputs from the console for the updation of record.
	 * 
	 * @return the identity for the readIdentityUpdateFromConsole() method
	 */
	public Identity readIdentityUpdateFromConsole() {
		final Identity identity = new Identity();
		logger.info("Please enter the new Display Name: ");
		String line = scanner.nextLine();
		identity.setDisplayName(line);
		logger.info("Please enter the new Email ID: ");
		line = scanner.nextLine();
		identity.setEmail(line);
		logger.info("Please enter the Existing UID: ");
		line = scanner.nextLine();
		identity.setUid(line);
		return identity;
	}

	/**
	 * Getting the user inputs from the console for deletion of record.
	 * 
	 * @return the identity for the readUIDfromConsole() method
	 */
	public Identity readUIDfromConsole() {
		logger.info("Enter UID for deletion: ");
		final Identity identity = new Identity();
		String line = scanner.nextLine();
		identity.setUid(line);

		return identity;
	}

	/**
	 * Getting the user inputs from the console for the username and password.
	 * 
	 * @return the loginIdentity for the readLoginfromConsole() method
	 */
	public LoginIdentity readLoginfromConsole() {
		final LoginIdentity loginIdentity = new LoginIdentity();
		logger.info("\nEnter Username:");
		String line = scanner.nextLine();
		loginIdentity.setUsername(line);
		logger.info("\nEnter Password:");
		line = scanner.nextLine();
		loginIdentity.setPassword(line);
		return loginIdentity;
	}

	/**
	 * This method is used to display the identities.
	 * 
	 * @param identities
	 */
	public void displayIdentitiesInConsole(List<Identity> identities) {
		int i = 1;
		for (final Identity identity : identities) {
			logger.info(i++ + " -" + identity);
		}
	}

	/**
	 * Displaying the Menu
	 */
	public void displayMenu() {
		logger.info(START_TAG);
		logger.info("*       Menu         *");
		logger.info(FINAL_TAG);
		logger.info("\n1 - Create Identity");
		logger.info("\n2 - Search Identity");
		logger.info("\n3 - Update Identity");
		logger.info("\n4 - Delete Identity");
		logger.info("\n5 - Exit");
	}

	/**
	 * Menu Operations to be performed by the user.
	 */
	public void userOperationMenu() {
		final Identity identity;
		final Identity criteria;
		final IdentityJDBCDAO dao = new IdentityJDBCDAO();
		logger.info("\nPress the operation to perform: ");
		final String menuInput = scanner.nextLine();

		switch (menuInput) {
		// Create Record
		case "1":
			identity = readIdentityFromConsole();
			dao.create(identity);
			userContinueOption();
			break;
		// Search Record
		case "2":
			criteria = readCriteriaFromConsole();
			final List<Identity> resultList = dao.search(criteria);
			displayIdentitiesInConsole(resultList);
			userContinueOption();
			break;
		// Update Record
		case "3":
			identity = readIdentityUpdateFromConsole();
			dao.update(identity);
			userContinueOption();
			break;
		// Delete Record
		case "4":
			identity = readUIDfromConsole();
			dao.delete(identity);
			userContinueOption();
			break;
		// Exit the menu
		case "5":
			userContinueOption();
			break;
		// Default operations
		default:
			logger.info("\nWrong Operation, Please enter the correct option: \n");
			displayMenu();
			userOperationMenu();
		}

	}

	/**
	 * This method used to continue or exit the program.
	 */
	public void userContinueOption() {
		logger.info("Do you want to exit. Press Y or N ?");
		final String userContinueVal = scanner.nextLine();

		switch (userContinueVal) {
		case "y":
		case "Y":
			logger.info("**********************");
			logger.info("*        Bye         *");
			logger.info("**********************");
			break;
		case "n":
		case "N":
			displayMenu();
			userOperationMenu();
			break;
		default:
			logger.info("\nWrong Operation, Please enter the correct option: \n");
			displayMenu();
			userOperationMenu();
		}
	}

	/**
	 * This method used to display the welcome message.
	 */
	public void welcomeProject() {
		logger.info("*************************************");
		logger.info("*                                   *");
		logger.info("*      Welcome to IAM PROJECT       *");
		logger.info("*                                   *");
		logger.info("*************************************");
	}

	/**
	 * This method used to close the scanner.
	 */
	public void releaseResources() {
		scanner.close();
	}

}